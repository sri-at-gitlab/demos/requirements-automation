const assert = require('assert').strict;
const fs = require('fs');

const test_report_file_name = 'test_report.json';

const test_case_one = () => true;
const test_case_two = () => true;
const test_case_three = () => true;
const test_case_forty_nine = () => true;
const test_case_five = () => true;

const test_suite = [
    test_case_one, 
    test_case_two, 
    test_case_three,
    test_case_forty_nine,
    test_case_five,
];

const main = () => {
    const result = {};
    test_suite.forEach(test_case => {
        const test_case_name = test_case.name;
        const test_case_result = test_case();
        if (!test_case_result) console.error(`${test_case_name} failed`);
        else console.log(`${test_case_name} passed`);
        return result[test_case_name] = test_case_result ? 'passed' : 'failed'
    });
    fs.writeFileSync(test_report_file_name, JSON.stringify(result), 'utf8');
};

main();
