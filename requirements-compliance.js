const fs = require('fs');
const csv_parse_sync = require('csv-parse/lib/sync');

const path_traceability_matrix = 'traceability_matrix.csv';
const path_test_report = 'test_report.json';
const path_requirements_compliance_report = 'requirements_compliance_report.json';

if (!fs.existsSync(path_traceability_matrix)) {
    console.error(`${path_traceability_matrix} not found`);
    process.exit(1);
}

if (!fs.existsSync(path_test_report)) {
    console.error(`${path_test_report} not found`);
    process.exit(1);
}

const traceability_matrix = csv_parse_sync(fs.readFileSync(path_traceability_matrix), {from_line: 2});
const test_report = JSON.parse(fs.readFileSync(path_test_report));;

const requirements_compliance_report = {};
traceability_matrix.forEach(row => {
    const requirement_id = row[0].replace('REQ-', '');
    const test_case_name = row[1];
    requirements_compliance_report[requirement_id] = test_report[test_case_name];
});

console.log(requirements_compliance_report);
fs.writeFileSync(path_requirements_compliance_report, 
                 JSON.stringify(requirements_compliance_report), 
                 'utf-8');