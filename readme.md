# Requirements Management and Automation

Requirement Management in GitLab is geared towards automation, compliance and verifiability.

The actions you may perform in the `Project -> Requirements` page is the following:
- Requirements CRUD
    - List / Read
    - Create
    - Update
    - Delete
- Import / Export

## Traceability Matrix

```
In software development, a traceability matrix (TM) is a document, usually in the form of a table, 
used to assist in determining the completeness of a relationship by correlating any two baselined 
documents using a many-to-many relationship comparison.[1]:3–22 It is often used with high-level 
requirements (these often consist of marketing requirements) and detailed requirements of the product 
to the matching parts of high-level design, detailed design, test plan, and test cases.

 -- https://en.wikipedia.org/wiki/Traceability_matrix
```

![Screenshot of traceability matrix in a spreadsheet form](https://imgur.com/LAz9QpJ.png)

```csv
Requirement ID,     Test Case
REQ-1,              test_case_three
REQ-2,              test_case_two
REQ-3,              test_case_one
```

- [traceability-matrix.csv](./traceability-matrix.csv)

## Test Suite

This demo project contains a test suite with three sample test cases.

Execution of tests produces a test report of the format:

```json
{
    "test_case_one"  :  "passed",
    "test_case_two"  :  "passed",
    "test_case_three":  "failed"
}
```

## Mechanics of Requirements Automation

1. Read traceability matrix, it must exist at source, else automation fails
1. Execute test suite, generate test results report
1. Execute requirements compliance which matches test results with traceability matrix
1. Generate and publish requirements compliance report

## Requirements Compliance Reporting

```json
    {
        "REQ_ID": "state",
        // ..
    }
```

![Requirements result screenshot](https://imgur.com/Ybz8Xqe.png)

- View results
- Publish as artifact
- Export as CSV